﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Definitions.Entities
{
    public class Option
    {
        public string Name { get; }

        public Option(string _name)
        {
            this.Name = _name;
        }

    }
}
