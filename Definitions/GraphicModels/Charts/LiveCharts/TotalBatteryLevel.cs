﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Data_Visualization_Tool.Definitions.GraphicModels.Charts
{
    public class TotalBatteryLevel
    {
        public double Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        public TotalBatteryLevel()
        {
            Value = 22;
            Formatter = x => x + "% KWh";
        }

        private void MoveOnClick(object sender, RoutedEventArgs e)
        {
            Value = new Random().Next(50, 100);
        }

        public Func<double, string> Formatter { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private double _value;
    }
}
