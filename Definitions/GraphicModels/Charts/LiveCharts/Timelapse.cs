﻿using LiveCharts;
using LiveCharts.Uwp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;

namespace Data_Visualization_Tool.Definitions.GraphicModels.Charts
{
    public class Timelapse
    {
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }
        public LiveCharts.Uwp.LineSeries Series { get; set; }

        public Timelapse(int _limit = 10)
        {
            Series = new LiveCharts.Uwp.LineSeries
            {
                Title = "Battery Change",
                Values = GenerateRandoms(_limit)
            };

            Labels = new[] { "Jan-12 : 0400", "Jan-12 : 0500" };
            YFormatter = value => value.ToString("C");

            this.Series.LineSmoothness = 1;

            SeriesCollection = new SeriesCollection();

            SeriesCollection.Add(Series);

        }


        private int displayLimit;

        private ChartValues<double> GenerateRandoms(int _limit)
        {
            displayLimit = _limit;
            ChartValues<double> randData = new ChartValues<double>();
            Random rnd = new Random();
            for (int i = 0; i < _limit; i++)
                randData.Add(rnd.Next(0, 101) * 1.0d);
            return randData;
        }
    }
}
