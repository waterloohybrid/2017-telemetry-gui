﻿using Syncfusion.UI.Xaml.Gauges;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Definitions.GraphicModels.Gauges
{
    [DataContract]
    public class SpeedometerGauge
    {
        [DataMember]
        public SfCircularGauge Gauge { get; set; }
        [DataMember]
        public CircularScale Scale { get; }
        [DataMember]
        public double Speed { get { return Speed; }
            set { Speed = (value >= Scale.StartValue && value <= Scale.EndValue) ? value : 0f; } }

        public SpeedometerGauge(int _low = 0, int _high = 0)
        {
            //Order matters here
            Gauge = new SfCircularGauge();
            Scale = new CircularScale();
            Speed = 0f;
        }

        public void ChangeSpeedometerRange(int _low, int _high)
        {
            Scale.StartValue = _low;
            Scale.EndValue = _high;
        }
    }
}
