﻿using Data_Visualization_Tool.Definitions.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Definitions.Vehicle
{
    public class BatteryCell
    {
        public int CellNumber { get; set; }
        public decimal BatteryLevel { get; set; }

        public string Label { get; set; }
        public CellStateEnum CellState { get; set; }

        public BatteryCell(int _cellNum, decimal _batteryLevel, string _label = "")
        {
            CellNumber = _cellNum;
            BatteryLevel = _batteryLevel;
            Label = Label == "" ? $"Cell #{CellNumber}" : _label;
            OnCreateUpdateBatteryState(BatteryLevel);
        }

        public void UpdateBatteryLevel(CellStateEnum _state)
        {
            switch (_state)
            {
                case (CellStateEnum.FullyCharged):
                    BatteryLevel = 100;
                    break;
                default:
                    BatteryLevel = 0;
                    break;
            }
        }

        private void OnCreateUpdateBatteryState(decimal _cell)
        {
            if (_cell == 100)
                CellState = CellStateEnum.FullyCharged;
            else
                CellState = CellStateEnum.Discharged;
        }
    }
}
