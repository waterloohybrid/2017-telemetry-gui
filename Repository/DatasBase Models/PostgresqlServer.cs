﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Repository.DatasBase_Models
{
    [DataContract(Name = "WFE Postgresql Server")]
    public class PostgresqlServer
    {
        [DataMember]
        public string hostServer { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        private string password { get; set; }
        [DataMember]
        public string database { get; set; }

        public PostgresqlServer(string _host, string _username, string _password, string _database)
        {
            hostServer = _host;
            username = _username;
            password = _password;
            database = _database;
        }

        public void ChangePassword(string _pwd)
        {
            password = _pwd;
        }

        [DataMember]
        public string ConnectionString { get {
                return $"Host={hostServer};Username={username};Password={password};Database={database}";
            }  }

    }
}
