﻿using Repository.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.IRepository
{
    public interface IRepository
    {
        DBConnectionStatus Connect();

        DBConnectionStatus CheckConnectionStatus();

        DBConnectionStatus FailSafe();

        DBConnectionStatus Disconnect();

        DBTransactionStatus Commit();
    }
}
