﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Postgresql.TableObjects
{
    class VehicleRunCycle
    {
        public VehicleRunCycle(int _cycleId, string _runTime, DateTime _startTime, DateTime _endTime, string _errors, int _startBattery, int _endBattery)
        {
            this.cycleId = _cycleId;
            this.runType = _runTime;
            this.startTime = _startTime;
            this.endTime = _endTime;
            this.errors = _errors;
            this.startBattery = _startBattery;
            this.endBattery = _endBattery;
        }

        private int cycleId { get; set; }
        private string runType { get; set; }
        private DateTime startTime { get; set; }
        private DateTime endTime { get; set; }
        private string errors { get; set; }
        private int startBattery { get; set; }
        private int endBattery { get; set; } 
    }
}
