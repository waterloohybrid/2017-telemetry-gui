﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Postgresql.TableObjects
{
    class SensorData
    {
        public SensorData(int _dataId, int _cycleId, int _device_id, int _vehicleState, float _dataField, DateTime _timeStamp)
        {
            this.dataId = _dataId;
            this.cycleId = _cycleId;
            this.deviceId = _device_id;
            this.vehicleState = _vehicleState;
            this.dataField = _dataField;
            this.timeStamp = _timeStamp;
        }

        private int dataId { get; set; }
        private int cycleId { get; set; }
        private int deviceId { get; set; }
        private int vehicleState { get; set; }
        private float dataField { get; set; }
        private DateTime timeStamp { get; set; }

    }
}
