﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Postgresql.TableObjects
{
    class SensorDevice
    {
        public SensorDevice(int _device_int, int _type_id, string _name, int _state, bool _isDigital, string _location, DateTime _dateLastActive, DateTime _dateInstalled)
        {
            this.deviceId = _device_int;
            this.typeId = _type_id;
            this.name = _name;
            this.state = _state;
            this.isDigital = _isDigital;
            this.location = _location;
            this.dateLastActive = _dateLastActive;
            this.dateInstalled = _dateInstalled;
        }

        private int deviceId { get; set; }
        private int typeId { get; set; }
        private string name { get; set; }
        private int state { get; set; }
        private bool isDigital { get; set; }
        private string location { get; set; }
        private DateTime dateLastActive { get; set; }
        private DateTime dateInstalled { get; set; }

    }
}
