﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Postgresql.TableObjects
{
    class SensorType
    {
        public SensorType(int _typeId, string _type, string _name, string _description)
        {
            this.typeId = _typeId;
            this.type = _type;
            this.name = _name;
            this.description = _description;
        }

        private int typeId { get; set; }
        private string type { get; set; }
        private string name { get; set; }
        private string description { get; set; }
    }
}
