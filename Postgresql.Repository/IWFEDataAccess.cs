﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Postgresql.Repository
{
    //Below is a attribute used in .Net for WCF - an attribute centric programming model
    [ServiceContract]
    public interface IWFEDataAccess
    {
        [OperationContract]
        Tuple<int, string> GetBatteryCellTemperature();

        [OperationContract]
        void ConnectToPostgresql();

        [OperationContract]
        void DisconnectPostgesql();
    }
}
