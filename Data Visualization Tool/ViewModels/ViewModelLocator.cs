﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.ViewModels
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary> 
    public class ViewModelLocator
    {
        public const string HomePageKey = "HomePage";
        public const string KinematicsPageKey = "KinematicsPageKey";
        public const string BatteryVisionPageKey = "BatteryVision";

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {            
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            NavigationService nav = new NavigationService();
            nav.Configure(HomePageKey, typeof(Home));
            nav.Configure(KinematicsPageKey, typeof(Views.KinematicsVision));
            nav.Configure(BatteryVisionPageKey, typeof(Views.BatteryVision));

            if (ViewModelBase.IsInDesignModeStatic)
            {
                // Create design time view services and models
            }
            else
            {
                // Create run time view services and models
            }

            //Register your services used here
            SimpleIoc.Default.Register<INavigationService>(() => nav);
            SimpleIoc.Default.Register<HomePageViewModel>();
            SimpleIoc.Default.Register<KinematicsPageViewModel>();
            SimpleIoc.Default.Register<BatteryPageViewModel>();
        }


        // <summary>
        // Gets the HomePage view model.
        // </summary>
        // <value>
        // The HomePage view model.
        // </value>
        public HomePageViewModel HomePageInstance
        {
            get
            {
                return ServiceLocator.Current.GetInstance<HomePageViewModel>();
            }
        }

        /// <summary>
        /// Gets the Kinematics Page view model
        /// </summary>
        public KinematicsPageViewModel KinematicsPageInstance
        {
            get
            {
                return ServiceLocator.Current.GetInstance<KinematicsPageViewModel>();
            }
        }

        // <summary>
        // The cleanup.
        // </summary>
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }

}