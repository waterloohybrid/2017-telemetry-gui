﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Data_Visualization_Tool.Services.NavigationService;
using GalaSoft.MvvmLight.Command;

namespace Data_Visualization_Tool.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {
        public HomePageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            Title = "Home Page";
            NavigateCommand = new RelayCommand(NavigateCommandAction);
        }

        private void NavigateCommandAction()
        {
            //Do Something
            //_navigationService.NavgiateTo("KinematicsPageKey");
           
        }

        private readonly INavigationService _navigationService;
        public RelayCommand NavigateCommand { get; private set; }
        private bool _isLoading = false;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                _isLoading = value;
                RaisePropertyChanged("IsLoading");

            }
        }
        private string _title;
        public string Title
        {

            get
            {
                return _title;
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    RaisePropertyChanged("Title");
                }
            }
        }

        public HomePageViewModel()
        {
            Title = "Hello Joel";
        }
    }
}
