﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using LiveCharts;
using LiveCharts.Uwp;
using System.Threading.Tasks;
using Data_Visualization_Tool.Definitions.GraphicModels.Charts;
using Syncfusion.UI.Xaml.Charts;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Data_Visualization_Tool.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.TempChart = new BatteryTemperatureChart(true, 100);

            Syncfusion.UI.Xaml.Charts.ColumnSeries CellTemps = new Syncfusion.UI.Xaml.Charts.ColumnSeries();
            CellTemps.ItemsSource = TempChart.Cells;
            CellTemps.XBindingPath = "CellNumber";
            CellTemps.YBindingPath = "BatteryLevel";

            TemperatureChart.Series.Add(CellTemps);

            battery = new TotalBatteryLevel();
            time = new Timelapse(10);
            DataContext = this;

        }

        public BatteryTemperatureChart TempChart { get; set; }

        public TotalBatteryLevel battery { get; set; }

        public Timelapse time { get; set; }

    }
}
