﻿using System;

namespace Data_Visualization_Tool.Platform
{
    /// <summary>
    /// EventArgs holding a type
    /// </summary>
    public class TypeEventArgs : EventArgs
    {
        public readonly Type Type;

        public TypeEventArgs(Type type)
        {
            this.Type = type;
        }
    }
}
