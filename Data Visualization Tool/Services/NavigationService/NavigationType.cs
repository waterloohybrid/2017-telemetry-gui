﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Services.NavigationService
{
    public enum NavigationType
    {
        /// <summary>
        /// Navigating forward to new page.
        /// </summary>
        Forward,

        /// <summary>
        /// Navigating backward to previous page.
        /// </summary>
        Backward
    }
}
