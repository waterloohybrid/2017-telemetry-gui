﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Services.NavigationService
{
    public interface INavigationService
    {
        /// <summary>
        /// The event Raised when navigation happens
        /// </summary>
        event EventHandler<NavigationEventArgs> Navigated;

        /// <summary>
        /// Get the type of Active View Model
        /// </summary>
        Type ActiveViewModelType { get; }

        /// <summary>
        /// Navigate to a given ViewModel and pass some data to model.
        /// </summary>
        /// <param name="_viewModelType"> Type of the ViewModel to navigate to.</param>
        /// <param name="data"> The data to be passed to target ViewModel. </param>
        /// <param name="isRoot"> Specifies if the target view should be set as the origion of navigation. </param>
        /// <returns></returns>
        Task<bool> NavigateAsync(Type _viewModelType, object data, bool isRoot = false);

        /// <summary>
        /// Navigate to given ViewModel
        /// </summary>
        /// <param name="_viewModelType"></param>
        /// <param name="isRoot"></param>
        /// <returns></returns>
        Task<bool> NavigateAsync(Type _viewModelType, bool isRoot = false);

        /// <summary>
        /// Return control back to the previous ViewModel
        /// </summary>
        /// <returns></returns>
        Task<bool> GoBackAsync();

        /// <summary>
        /// Return control back to the previous page with specified Data
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        Task<bool> GoBackAsync(object result);

        /// <summary>
        /// Presist Current Application State.
        /// </summary>
        /// <returns></returns>
        Task PersistApplicationStateAsync();

        /// <summary>
        /// Restore Presisted application state.
        /// </summary>
        /// <returns></returns>
        Task RestoreApplicationStateAsync();
    }
}
